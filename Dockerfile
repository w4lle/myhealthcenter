FROM python:3.6

MAINTAINER Saleh Hindi <saleh.hindi.one@gmail.com>

# Configure environment
ENV HOME=/home/work    

# Install some system dependencies
RUN apt-get update -y && apt-get install -y build-essential libssl-dev \
    python3-dev vim

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y nodejs
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -; \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list; \
    apt-get update; \
    apt-get install -y yarn

# Create working directory
RUN mkdir $HOME
RUN mkdir $HOME/myhealthcenter
WORKDIR $HOME/myhealthcenter
COPY ./ ./

# Install python requirements
RUN pip3 install -r requirements.txt

# Yarn and yarn build
RUN yarn && yarn build

EXPOSE 8000