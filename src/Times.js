import React, { Component } from 'react';

class Times extends Component {
  constructor(props) {
    super(props);
    this.state = {};


    this.onClickAppointmentTime = this.onClickAppointmentTime.bind(this);
  }

  onClickAppointmentTime(time) {
    console.log(time)
    window.location = `${this.getBaseUrl()}/success`

    // Takes you to a new page
  }

  getBaseUrl() {
    var baseUrl;
    if (window.location.port === "") {
      baseUrl = `${window.location.protocol}//${window.location.hostname}`;
    } else {
      baseUrl = `${window.location.protocol}//${window.location.hostname}:${window.location.port}`;
    }
    return baseUrl;
  }

  // appointmentTimes should be its own component
  render() {
    var times = [];
    for (var i=0; i<this.props.availableTimes.length; i++) {
      const availableTime = this.props.availableTimes[i];
      times.push(
        <div onClick={ () => this.onClickAppointmentTime(availableTime) } className="time">
          <span>{availableTime}</span>
        </div>
      ) 
    }

    return (
      <div className="times">
        {times}
      </div>
    )
  }
}

export default Times;
