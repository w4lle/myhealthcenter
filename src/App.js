import React, { Component } from 'react';
import { Route, Router, NavLink, BrowserRouter, Switch } from "react-router-dom";

import FrontPage from './FrontPage.js';
import Calendar from './Calendar.js';
import ErrorPage from './ErrorPage.js';
import SuccessPage from './SuccessPage.js';

import './App.css';

class App extends Component {
  render() {
    return (
  		<BrowserRouter>
    		<Switch>
      		<Route path="/calendar" component={Calendar}/>
      		<Route path="/success" component={SuccessPage}/>
          <Route path="*" component={FrontPage}/>
    		</Switch>
  		</BrowserRouter>
    )
  }
}

export default App;