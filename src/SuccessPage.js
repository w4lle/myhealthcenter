import React, { Component } from 'react';

import './SuccessPage.css'

class SuccessPage extends Component {
  constructor(props) {
    super(props)

    this.onYesClick = this.onYesClick.bind(this);
    this.onNoClick = this.onNoClick.bind(this);
  }

  logData(data) {
    const url = `${this.getBaseUrl()}/api/log/${data}/`;
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstParam: 'yourValue',
        secondParam: 'yourOtherValue',
      })
    });
  }

  getBaseUrl() {
    var baseUrl;
    if (window.location.port === "") {
      baseUrl = `${window.location.protocol}//${window.location.hostname}`;
    } else {
      baseUrl = `${window.location.protocol}//${window.location.hostname}:${window.location.port}`;
    }
    return baseUrl;
  }

  onYesClick() {
    console.log("YESSSS")
    this.logData('yes')
    // Send request to server logging IP, time of day, baseURL, sha/version of app
  }

  onNoClick() {
    console.log("BOOOOO")
    this.logData('no')
    // Send request to server logging IP, time of day, anything else we can
  }

  render() {
    return (
      <div className='pageContainer'>
        <div className='question'>Do you want to know when you're covered?</div>
        <div className='answers'>
          <div className='yes' onClick={this.onYesClick}>Yes</div>
          <div className='no' onClick={this.onNoClick}>No</div>
        </div>
      </div>
    )
  }
}

export default SuccessPage;
