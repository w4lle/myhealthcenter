import React, { Component } from 'react';
import Select from 'react-select';

import styles from './CollegeDropdown.css';
import 'react-select/dist/react-select.css';

class CollegeCard extends Component {
  state = {
    selectedOption: '',
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    // selectedOption can be null when the `x` (close) button is clicked
    if (selectedOption) {
      console.log(`Selected: ${selectedOption.label}`);
    }
  }

  render() {
    const { selectedOption } = this.state;

    return (
      <li className="card school">
        <span>Where do you go to school?</span>
        <Select
          name="form-field-name"
          className="school-dropdown"
          value={selectedOption}
          onChange={this.handleChange}
          style={styles['Select-control']}
          placeholder="Select School"
          menuContainerStyle={styles['menuContainer']}

          options={[
            { value: 'No School', label: 'No School' },
            { value: 'Haverford', label: 'Haverford' },
            { value: 'Saleh School of Design', label: 'Salehs School of Design' },
          ]}
        />
      </li>
    )
  }
}

export default CollegeCard;