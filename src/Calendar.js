import React, { Component } from 'react';
import moment from 'moment';

import Bubble from './Bubble.js';
import Times from './Times.js';

import './Calendar.css'

// Should have 7 columns with 5 days for 35 dates
class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDay: false, 
      dayAvailability: {},
    };
  }

  componentDidMount() {
    this.getAvailability();
  }

  getAvailability() {
    const url = `${this.getBaseUrl()}/api/availability/`;
    fetch(url, {
      "credentials":"omit",
      "headers":{},
      "referrer":"https://l.messenger.com/",
      "referrerPolicy":"origin",
      "method":"GET",
      "mode":"cors",
      "dataType": 'jsonp'
    }).then((response) => {
      return response.json()
    }).then((response) => {
      var models = {};
      for (var key in response) {
        var temp = [];
        for (var i = 0; i < response[key].length; i++) {
          temp.push(this.hourToString(response[key][i]));
        }
        models[key] = temp;
      }
      this.setState({dayAvailability: models})
      this.forceUpdate()
    });
  }

  // Converts from 24-hour format to 12-hour format
  hourToString(hour) {
  	var res = parseInt(hour);
  	if (res > 12) {
  		return `${res%12}pm`;
  	} else {
  		return `${res}am`;
  	}
  }

  //Get availabilityDays
  getAvailableDays() {
    const avail = this.state.dayAvailability;
    var res = [];
    for (var key in avail) {
      if (avail[key].length > 0) {
        res.push(parseInt(key));
      }
    }
    return res;
  }

	renderBubbles() {		
    const availableDays = this.getAvailableDays();

    var bubbles = []
    var date = moment("2018-09-01");
    // const accessDate = moment().date();

    // A Calendar component is a grid of 5x7 bubbles (there are at most 31 days in a month)
    // We iterate over each of the [1,2,3,4,5] rows of the grid,
    // and for each row we iterate over the columns of the calendar (Sun, Mond, ..., Sat)
    var dayOfTheMonth = 1;
		for (var row=1; row <= 6; row++) {
      // The first row is a special row because the first day of the month (June 1) might not start
      // on the first day of the week so we have to render invisible bubbles to take up
      // space on the layout
			if (row === 1) {
        var dow = date.day(); // converts the current day eg 2018-06-01 to a day of the week in 0-6

        // first we render the invisible days of the first week, ie the days not in the current month
			 	for (var i=0; i<dow; i++) {
		   		bubbles.push(
		   			<Bubble 
			   			id="0"
		   				className="day" 
		   				enabled={false} 
		   				dateNumber="" 
	   				/>
   				)
				}

        // then we render the visible days of the first week within the current month (like june 1, june 2, ...)
				for (var i=dow; i<7; i++) {
          const onBubbleClick = this.onBubbleClick.bind(this, dayOfTheMonth, availableDays.includes(dayOfTheMonth))
					bubbles.push(
						<Bubble 
							id={dayOfTheMonth}
							className="day"
              onClick={ onBubbleClick } 
							enabled={true} 
							dateNumber={dayOfTheMonth} 
							available={ availableDays.includes(dayOfTheMonth) } 
            />
					)
					dayOfTheMonth+=1
				} 
			}

      // Then we iterate over the non special weeks of the month
			if (row != 1 && row != 5) {
			  for (var dayNumber=0; dayNumber < 7; dayNumber++) {
					if (dayOfTheMonth > date.daysInMonth()) { break; }
          const onBubbleClick = this.onBubbleClick.bind(this, dayOfTheMonth, availableDays.includes(dayOfTheMonth))
          bubbles.push(
			    	<Bubble 
			    		id={dayOfTheMonth} 
			    		className="day"
              onClick={ onBubbleClick } 
			    		enabled={true} 
			    		dateNumber={dayOfTheMonth} 
              available={ availableDays.includes(dayOfTheMonth) }
			    		/>
		    		)
			    dayOfTheMonth+=1
			  }
			}
		}

		return bubbles;
	}

  getBaseUrl() {
    var baseUrl;
    if (window.location.port === "") {
      baseUrl = `${window.location.protocol}//${window.location.hostname}`;
    } else {
      baseUrl = `${window.location.protocol}//${window.location.hostname}:${window.location.port}`;
    }
    return baseUrl;
  }

  // Note: dayOfMonth is the last dayOfMonth rendered, not the last dayOfMonth clicked 
  onBubbleClick(dayOfTheMonth, available) {
    if (available) {
      this.setState({selectedDay: dayOfTheMonth});
    } else {
      this.setState({selectedDay: false});
    }
  }

	render() {
		return (
      <div className="Calendar">
				<div className="header2">
					<span>september</span>
				</div>

				<div className="month">
					<ul className="week-days">
						<li>sun</li>
						<li>mon</li>
						<li>tues</li>
						<li>wed</li>
						<li>thu</li>
						<li>fri</li>
						<li>sat</li>
					</ul>
					<ul className="days">
		      	{ this.renderBubbles() }
            { this.state.selectedDay && <Times availableTimes={ this.state.dayAvailability[this.state.selectedDay] }/> }
					</ul>
				</div>
      </div>
		)
	}
}

export default Calendar;