import React, { Component } from 'react';
import SubmitCard from './SubmitCard.js';
import CollegeCard from './CollegeCard.js';
import InsuranceCard from './InsuranceCard.js';
import DateCard from './DateCard.js';

import './FrontPage.css';

class FrontPage extends Component {
  constructor (props) {
    super(props)
    this.state = {nurses: [], insurances: []};
    // State should be state={nurses: this.props.nurses, insurances= this.props.insurances, _availability: this.props.availability }
  }

  componentDidMount() {
    this.getData('nurse');
    this.getData('insurance');
  }

  getBaseUrl() {
    var baseUrl;
    if (window.location.port === "") {
      baseUrl = `${window.location.protocol}//${window.location.hostname}`;
    } else {
      baseUrl = `${window.location.protocol}//${window.location.hostname}:${window.location.port}`;
    }
    return baseUrl;
  }

  getData(model) {
    const url = `${this.getBaseUrl()}/api/${model}s/`;
    fetch(url, {
      "credentials":"omit",
      "headers":{},
      "referrer":"https://l.messenger.com/",
      "referrerPolicy":"origin",
      "method":"GET",
      "mode":"cors",
      "dataType": 'jsonp'
    }).then((response) => {
      return response.json()
    }).then((response) => {
      const unformattedResponse = response;
      const models = unformattedResponse.map((responseModel) => {
        return {id: responseModel.pk, name: responseModel.fields[`${model}_name`],}
      });

      if (model === 'nurse') {
        this.setState({nurses: models})
      } else if (model === 'insurance') {
        this.setState({insurances: models})
      }

      this.forceUpdate()
    });
  }

  getCalendarLink() {
    var calendarUrl = this.getBaseUrl()+"/calendar";
    return calendarUrl;
  }

  render() {
    return (
      <div className="cards-container">
        <div className="header-card">
          <div className="header-text">
            Is your nurse in your network?
          </div>
        </div>

        <div className="controls">
          <div className="school">
            <span>Name</span>
          </div>
          <div className="insurances">
            <InsuranceCard insurances={ this.state.insurances } />
          </div>
          <div className="go"><a href={this.getCalendarLink()}>Find Out</a></div>
        </div>
      </div>
    );
  }
}

export default FrontPage;
