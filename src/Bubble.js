import React, { Component } from 'react';

import './Bubble.css'

import nurse from './nurse.svg'

class Bubble extends Component {
	// Props: empty T/F,CalendarDate (1,2,3, ...), nurses [{NURSE}, {NURSE}]

  constructor(props) {
    super(props)
    this.props = props;
  }

  renderAvailableBubble(id) {
		if (this.props.available) {
			return (
				<div className="available availableBubble">
					<img className="available" src={nurse} alt="An in network nurse is available today" />
  			</div>
			)
		}
  }

	render() {
		const enabledClass = this.props.enabled ? "Bubble days" : "Bubble days empty"
		const className = `${enabledClass} ${this.props.className}`;

		return (
      <li id={this.props.id} onClick={this.props.onClick} className={className}>
      	{ this.renderAvailableBubble(this.props.id) }
	      <div className="calendar-number">{this.props.dateNumber}</div>
      </li>
		)
	}
}

export default Bubble;