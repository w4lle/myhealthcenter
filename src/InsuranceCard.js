import React, { Component } from 'react';
import Select from 'react-select';

import styles from './InsuranceDropdown.css';

class InsuranceCard extends Component {
  constructor (props) {
    super(props)
    // this.state = {insurancs: this.props.insurances};
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    // selectedOption can be null when the `x` (close) button is clicked
    if (selectedOption) {
      console.log(`Selected: ${selectedOption.label}`);
    }
  }	

	render() {
    const { selectedOption } = this.state;

		return (
      <Select
        name="form-field-name2"
        className="insurance-dropdown"
        value={selectedOption}
        styles={styles['insurance-dropdown']}
        placeholder="Insurance"
        menuContainerStyle={styles['insurance-dropdown']}
        onChange={this.handleChange}
        options={ this.props.insurances.map(insurance => ({value: insurance.name, label: insurance.name})) }
      />
		)
	}
}

export default InsuranceCard;