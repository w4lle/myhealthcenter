import React, { Component } from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';

class DateCard extends Component {
  constructor (props) {
    super(props)
    this.state = {
      startDate: moment()
    };
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });
  }


	render() {
		return (
      <li className="card date-picker-card">
        DATE

        <DatePicker
          className="date-picker"
          selected={this.state.startDate}
          onChange={this.handleChange}
        />
      </li>
		)
	}
}

export default DateCard;