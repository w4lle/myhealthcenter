from __future__ import print_function
import datetime
from apiclient.discovery import build
from httplib2 import Http
from oauth2client import file as oauth_file, client, tools
from dateutil import parser
from tzinfo_examples import Pacific

# If modifying these scopes, delete the file token.json.
SCOPES = 'https://www.googleapis.com/auth/calendar.readonly'

def last_hour_of_month(any_day):
    """returns datetime object in the last hour of last day of the month"""
    next_month = any_day.replace(day=28) + datetime.timedelta(days=4)  # this will never fail
    res = next_month - datetime.timedelta(days=next_month.day)
    res = res.replace(hour=23)
    return res

def overlaps(datetime, busy):
    """Given a list of busy times [{datetime, datetime}], check if datetime is busy"""
    for entry in busy:
        if parser.parse(entry['start']) <= datetime < parser.parse(entry['end']):
            return True
    return False

def reverse_availability(busy, office_open, office_close, weekends_off=True):
    """Given a list of busy times ([{datetime, datetime}]) in a month, and office opening and closing times (ints), 
    returns list of available times ([datetime]). Returns availabilities in 1-hour increments"""
    end_of_week = 5 if weekends_off else 7
    if office_close < 12:
        office_close += 12
    first = parser.parse(busy[0]['start'])
    num_days = last_hour_of_month(first).day
    res = []
    clinic_timezone = Pacific # next step: pass this as an argument, e.g. api/availability/pacific
    for day in range(datetime.datetime.utcnow().day+1, num_days+1):
        for hour in range(office_open, office_close):
            candidate = datetime.datetime(first.year, first.month, day, hour, 0, 0, tzinfo=clinic_timezone)
            if candidate.weekday() < end_of_week and not overlaps(candidate, busy): #automatically take saturday and sunday off
                res.append(candidate)
    return res


def main():
    """Returns list of free datetimes."""
    store = oauth_file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets('credentials.json', SCOPES)
        creds = tools.run_flow(flow, store)
    service = build('calendar', 'v3', http=creds.authorize(Http()))

    # Call the Calendar API
    now = datetime.datetime.utcnow()
    monthEnd = last_hour_of_month(now)
    now = now.isoformat() + 'Z'  # 'Z' indicates UTC time
    monthEnd = monthEnd.isoformat() + 'Z'
    body = {
      "timeMin": now,
      "timeMax": monthEnd,
      "timeZone": 'US/Central',
      "items": [{"id": '5l64m4levif8akeqvkhrvn61a8@group.calendar.google.com'}]
    }
    eventsResult = service.freebusy().query(body=body).execute()
    busy = eventsResult['calendars']['5l64m4levif8akeqvkhrvn61a8@group.calendar.google.com']['busy']

    # Turn busy times into free times
    res = reverse_availability(busy, 9, 5)

    return res

if __name__ == '__main__':
    main()