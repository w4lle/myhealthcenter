from django.http import HttpResponseRedirect, Http404, HttpResponse
# from django.shortcuts import get_object_or_404, render
# from django.urls import reverse
# from django.views import generic
# import random
from django.utils import timezone
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
import json
import gcal
# from dateutil import parser


from .models import Nurse, Insurance, NurseInsurance, Visit, Availability

@csrf_exempt
def yes(request):
    Visit(interested=True, pub_date=timezone.now()).save()
    return HttpResponse("Thank you for responding!", content_type="text/plain")

@csrf_exempt
def no(request):
    Visit(interested=False, pub_date=timezone.now()).save()
    return HttpResponse("Thank you for responding!", content_type="text/plain")

def nurses(request):
    res = Nurse.objects.all()
    res = serializers.serialize('json', res)
    res = HttpResponse(res, content_type='application/json')
    res['Access-Control-Allow-Origin'] = "*"
    return res

def insurances(request):
    res = Insurance.objects.all()
    res = serializers.serialize('json', res)
    res = HttpResponse(res, content_type='application/json')
    res['Access-Control-Allow-Origin'] = "*"
    return res

def availability(request):
    """Returns http response with dictionary mapping from days (int) to times (list of strings)."""
    events = gcal.main() #should contain free times only in the same month
    res = {i:[] for i in range(1, 32)}
    for event in events:
        res[event.day] += [str(event.hour)]
    res = json.dumps(res)
    res = HttpResponse(res, content_type='application/json')
    res['Access-Control-Allow-Origin'] = "*"
    return res

