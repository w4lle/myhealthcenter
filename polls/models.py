import datetime

from django.db import models
from django.utils import timezone
# Create your models here.

class Nurse(models.Model):
    nurse_name = models.CharField(max_length=200)
    def __str__(self):
        return self.nurse_name

class Insurance(models.Model):
    insurance_name = models.CharField(max_length=200)
    def __str__(self):
        return self.insurance_name

class NurseInsurance(models.Model):
    nurse_id = models.ForeignKey(Nurse, on_delete=models.CASCADE)
    insurance_id = models.ForeignKey(Insurance, on_delete=models.CASCADE)

class Visit(models.Model):
	pub_date = models.DateTimeField('date published')
	interested = models.BooleanField()

class Availability(models.Model):
    start = models.DateTimeField('start_datetime')