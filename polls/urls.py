from django.urls import path
from django.conf.urls import url


from . import views

app_name = 'polls'
urlpatterns = [
    url(r'nurses/?', views.nurses, name='nurses'),
    url(r'insurances/?', views.insurances, name='insurances'),
    url(r'availability/?', views.availability, name='availability'),
    url(r'log/yes/?', views.yes, name='yes'),
    url(r'log/no/?', views.no, name='no')
]