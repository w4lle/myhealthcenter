# [TheHealthKiosk](https://www.thehealthkiosk.com/)
------

![Hello world](https://i.imgur.com/kaWo0F1.png)
![Hello world](https://i.imgur.com/pqiVziM.png)
![Hello world](https://i.imgur.com/D9PP5S9.png)


# Our Story

We created the app when we learned that not all nurses at Haverford accept all insurances approved by the college.  

When our friends went to the health center, they would randomly be charged a lot of money even though 
they had insurance approved by the college.  
We wanted some way for students to know that they are being covered when they go in for a visit,   
because students should be focusing on being healthy, not whether they are covered.

![Hello world](https://i.imgur.com/an6ae6c.png)
  
(Mobile Only. Open dev tools and select Mobile for desktop)  
# [VISIT OUR CALENDAR](https://www.thehealthkiosk.com/calendar)
It's made with love

# Running the app

## Running Using Docker (recommended)
If you have Docker installed, running the app is stupid easy. This is the recommended way to run the app. 

Build the container:
```
$ docker build -t myhealthcenter .
```

Run the container and then go to `localhost:8000`
```
$ docker run -it -p 8000:8000 myhealthcenter python manage.py runserver 0.0.0.0:8000
```

To mount a volume so you can edit files and have the changes appear in the docker container run
```
$ docker run -it -v /absolute/path/to/myhealthcenter/:/home/work/myhealthcenter -p 8000:8000 myhealthcenter python manage.py runserver 0.0.0.0:8000
```


## Frontend
Make sure you have `yarn 1.6.0` or above. Make sure you have `node 8.11` or above. Note, do you need to install react separately?   
```
$ yarn
$ yarn build
$ yarn start
```

## Backend
Make sure you are running `python3` with `pip3`. Make sure you have the latest `pipenv` installed via `pip3`.  
Make sure you fill out the `.env` file!


```
$ pipenv --python python3
$ pipenv install
$ pipenv run python3 manage.py runserver

```

Opening your web browser at `http://127.0.0.1:8000/site` should show you the site.

  
## localtunnel (Optional)  
```
$ sudo npm install -g localtunnel
```
  

## Running
May need to run 'pipenv shell' to make this work 
```
$ yarn build
$ python manage.py runserver
$ lt -p 8000
```  

# Our Team

Big big shoutout to the team that made this app happen. You guys were incredible to work with.

Saleh Hindi (dev)

Ching Li (dev)

Maria-Veronica Rojas (dev)

Adam Stambor (leader)



